[33mcommit d965921260026c1bec653281616022e37c18a8ef[m[33m ([m[1;36mHEAD -> [m[1;32mmaster[m[33m, [m[1;31morigin/master[m[33m, [m[1;31morigin/HEAD[m[33m)[m
Author: Alejandro Tellez <tellez.alejandro@gmail.com>
Date:   Tue Jul 14 08:43:19 2020 -0500

    Implementando principios de REST

[33mcommit 29a65ecaf75f15e11686df96ac8072af1ff29d99[m
Author: Alejandro Tellez <tellez.alejandro@gmail.com>
Date:   Sat Jul 11 18:13:20 2020 -0500

    Organizar en una aplicacion los diferentes flujos de navegacion

[33mcommit d7bc3f612d8f4ac3d5c258c5fab29ba80a9c57c2[m
Author: Alejandro Tellez <tellez.alejandro@gmail.com>
Date:   Sat Jul 11 15:54:24 2020 -0500

    Implementando manejo de rutas avanzado

[33mcommit dda0c241a05f09bd9a40a07738ba3283e745c40d[m
Author: Alejandro Tellez <tellez.alejandro@gmail.com>
Date:   Fri Jul 3 18:32:20 2020 -0500

    Implementando SPA totalmente reactiva
